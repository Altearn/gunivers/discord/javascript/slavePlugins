# PLUGINS
Ici, vous pouvez mettre les plugin fini (ou pas) mais a peu près fonctionnel. Le but est d'avoir le plus de plugin servant a tous et a rien. Si le plugin n'est pas finit merci de l'indiquer. N'esiter pas a metre un readme pour indiquer comment at a quoi sert votre plugin.

Voici le schema d'un plugin :
```
📁<nom>
├──📁events
│   └──📄(event).js (nom de l'event ex: message.js)
├──📄plugin.json
└──📁commands
    └──📄(command).js (nom de la commande ex: ping.js)
```
Voici un exemple de plugin.js :
```
{
    "name": "Exemple",
    "tag": "basePlus",
    "_comment": "le tag est le nom du fichier du plugin",
    "description": "Un plugin servant a rien."
}
```
Voici un exemple de commande :
```
//ping.js
module.exports.run = [
    main = (client, message, args) => {
        message.channel.send("pong");
    }
];

module.exports.help = {
    name: "ping",
    desc: "just reply pong",
    aliases: ["p"],
    usage: ""
}
```
Et un autre exemple de commande:
```
module.exports.run = [
    main = (client, message, args) => {
        message.reply(" syntaxe incorrecte => `mange <tomate|patate>`"); //message si la commande a pas d'args
    },
    patate = (client, message, args) => {
        message.reply("Je mange une patate"); // message quand on fera `mange patate`
    },
    tomate = (client, message, args) => {
        message.reply("Je mange une tomate"); // message quand on fera `mange tomate`
    }
];

module.exports.help = {
    name: "mange",
    desc: "je mange quoi ?",
    aliases: [""],
    subcommands: ["tomate", "patate"],
    usage: "<tomate|patate>"
}
```
Et un exemple d'event :
```
//ready.js
module.exports = async (client) => {
    console.log("bot on !");
}
```
