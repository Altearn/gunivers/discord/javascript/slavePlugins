const Discord = require('discord.js');

module.exports.run = [
    main = (client, message, args) => {
        const { commands : commandsAll, config } = client;

        const prefix = config.prefix || 's-';
        const groupsAll = commandsAll.map(c => c.help.group);
        const groups = new Set(groupsAll);
        //return console.log(groupsAll);
        const showAll = args[0] && args[0].toLowerCase() === 'all';
        const data = [];
        const embed = new Discord.MessageEmbed()
            .setColor('BLUE');
    
        if (args[0] && !showAll) {
            //return message.channel.send('cette fonctionnalité n\'est pas encore disponible');
            if (args.length === 1) {
                const commands = commandsAll.find(command => command.help.name === args[0]);
                if(!commands) return message.reply("Commande introuvable !");
                let help = `${commands.help.desc} ${commands.help.guildOnly ? ' (uniquement sur les serveurs)' : ''}
    
                    **Format :** \`${prefix}${commands.help.name}${commands.help.usage ? ` ${commands.help.usage}` : ``}\` ou \`@${client.user.tag} ${commands.help.name}${commands.help.usage ? ` ${commands.help.usage}` : ``}\``;
    
                if (commands.help.aliases.length > 0) help += `\n**Aliases:** ${commands.help.aliases.join(', ')}`;
                help += `\n**Plugin :** ${commands.help.tag}`;
                help += `\n**Groupe :** ${commands.help.group}`;
                help += `\n**Info suplémentaire :** ${commands.help.info || "*pas d'information suplémentaire*"}`
    
                //if (commands.details) help += `\n**Detail:** ${commands.details}`;
                //if (commands.examples) help += `\n**Exemple:**\n${commands.examples.join('\n')}`;
    
                embed
                    .setAuthor(commands.help.name.toUpperCase())
                    .setDescription(help.replace(/ or /g, ' ou '))
            }
        } else {
    
            groups.forEach(grp => {
                data.push(`\n**・${grp}**`);
                const groupCommande = commandsAll.filter(cmd => cmd.help.group === grp && !cmd.help.hide);
                groupCommande.forEach(cmd => {
                    data.push(`\`${cmd.help.name}\`: ${cmd.help.desc}`);
                });
            });
    
            embed
                .setDescription(
                    `
                    Pour lancer une commande, utilisez \`${prefix}command\` ou \`@${client.user.tag} command\`.
                    Par exemple, \`${prefix}help\` ou \`@${client.user.tag} help\`.
    
                    Utilisez \`${prefix}help <commande>\` pour voir les informations détaillées de la commande spécifiée.
    
                    __**Commandes disponibles :**__
                    ${data.join('\n')}`
                    
                        )
                ;
        }
    
        return message.channel.send(embed);
    }
];

module.exports.help = {
    name: "help",
    desc: "Fleme d'en faire",
    aliases: ["h"],
    usage: "[command]"
}