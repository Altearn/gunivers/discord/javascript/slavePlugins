module.exports.run = [(client, message, args) => {
    if (!args.slice(0).join (' ')) return message.reply('Need a message to send')
    message.channel.send(args.slice(0).join (' ')) 
    message.delete();
    }]
module.exports.help = {
    name: "say",
    tag: "base",
    desc: "Send a message with the bot",
    aliases: ["s"],
    usage: "[argument]"
  }