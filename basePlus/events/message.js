/*

Credits for this part go to TrétinV3#7056

*/

const gunibot = require("gunibot");
const { Collection } = require('discord.js')
const cooldowns = new Collection();
const escapeRegex = str => str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');

module.exports = async (client, message) => {
    // Prefix initialisation (later it while work by checking the guild config but now we hardcode it)
    message.prefix = 's-'

    // Regex that will look if the message start with the prefix or with a bot mention
    const prefixRegex = new RegExp(`^(<@!?${client.user.id}>|${escapeRegex(message.prefix)})\\s*`)
    if (!prefixRegex.test(message.content) || message.author.bot) return

    // Idk what this do but it's usefull
    const [, matchedPrefix] = message.content.match(prefixRegex)

    // We split the message in arguments
    const args = message.content.slice(matchedPrefix.length).trim().split(/ +/)
    // We recover the ccommand name by the first argument
    const commandName = args.shift().toLowerCase()

    // Get the command with his name or alias
    const command = client.commands.get(commandName)
        || client.commands.get(client.alias.get(commandName));

    // If no command return
    if (!command) return;

    // If its an owneronly command return if you're not the owner
    if (command.help.ownerOnly && message.author.id !== ownerId) {
        return message.reply('⛔ This command is owner-only!');
    }

    // If you're in dm return
    if (message.channel.type === 'dm') return


    // Check if you have the permission to do the command
    if (command.help.permissions) {
        const authorPerms = message.channel.permissionsFor(message.author);
        if (!authorPerms || !authorPerms.has(command.help.permissions)) {
            return message.reply(`⛔ You can't run this command!${true ? `\nYou need the permission : \`${command.help.permissions}\`` : ``}`);
        }
    }

    //======================== cooldowns ========================
    if (!cooldowns.has(command.help.name)) {
        cooldowns.set(command.help.name, new Collection());
    }
    const now = Date.now();
    const timestamps = cooldowns.get(command.help.name);
    const cooldownAmount = (command.help.cooldown || 3) * 1000;
    if (timestamps.has(message.author.id)) {
        const expirationTime = timestamps.get(message.author.id) + cooldownAmount;

        if (now < expirationTime) {
            const timeLeft = (expirationTime - now) / 1000;
            return message.reply(` please wait ${timeLeft.toFixed(1)} second(s) to run the command \`${command.help.name}\`.`);
        }
    }
    timestamps.set(message.author.id, now);
    setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);
    //======================== cooldowns ========================

    // Try to run the command and if there is a probleme, redirect to the first personne in the owners list
    try {
        gunibot.subCommands(client, message, command, args);
        console.log(`${command.help.name} was runned by ${message.author.username} ${message.channel.type !== 'dm' ? `on ${message.guild.name}` : `in DM`}`);
    } catch (error) {
        console.log(error);
        message.reply(` désolé mais il y a eu une erreur avec cette commande. Merci d'en parler a \`${client.users.cache.get(client.config.owners[0]).tag}\`.`);
    }
}