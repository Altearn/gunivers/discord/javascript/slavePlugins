const fs = require("fs");
const configFs = JSON.parse(fs.readFileSync('./plugins/roleReaction/config.json'));
//const config = require('../config.json');

module.exports.run = [
    main = (client, message, args) => {
        message.reply(" syntaxe incorrecte => `role-reaction <list|add|remove|modify>`"); //message si la commande a pas d'args
    },
    list = async (client, message, args) => {
        //return console.log(config);
        let text = 'Voici les différents message a role-reaction de ce bot :';
        const array = configFs.roleReaction;
        //return console.log(array);
        for (let i = 0; i < array.length; i++) {
            const rr = array[i];
            const role = message.guild.roles.cache.find(r => r.id === rr.roleId);
            text += `\n**${i + 1}.** ${role.name} -> emoji : ${rr.emojiName} message : https://discord.com/channels/${rr.path}`;
        }
        message.channel.send(text);
    },
    add = async (client, message, args) => {
        if (args.length !== 4) return message.reply(' syntaxte incorecte : `role-reaction add <emoji> <role id> <lien du message>`');
        try {
            message.react(args[1]);
        } catch {
            return message.reply(' je crois que l\'emoji est incorrecte');
        }
        const emojiName = args[1];
        const roleId = args[2];
        const path = args[3].split("/").slice(4).join("/");
        //return console.log(configFs);
        configFs.roleReaction.unshift({
            "emojiName": emojiName,
            "path": path,
            "roleId": roleId
        })
        console.log(configFs.roleReaction);
        //console.log(path);
        //db.get("roleReaction").push({ emojiName: emojiName, path: path, roleId: roleId}).write();
        fs.writeFile("./plugins/roleReaction/config.json", JSON.stringify(configFs), async (err) => {
            if (err) {
                console.log(err);
                message.channel.send(`❌ Je n'ai pas réussi a ajouter le role-réaction.`);
            }
            else {
                message.channel.send(`✅ role-réaction ajouté sur le message https://discord.com/channels/${path}`)
                let messageReact = await client.guilds.cache.get(message.guild.id).channels.cache.get(path.split("/")[1]).messages.fetch(path.split("/")[2]);//.react(roleReaction.emojiName);
                messageReact.react(emojiName);
            }
        });

    },
    remove = (client, message, args) => {
        if (!args[1]) return message.reply(` il manque l'index du role-reaction a enlever. Pour les avoir fais la commande \`role-reaction list\``);
        if (args[1] === NaN) return message.reply(" l'index doit etre un nombre valide");
        if (args[1] >= configFs.roleReaction.length) return message.reply(" l'index est un nombre trop grand");
        const rr = configFs.roleReaction[args[1] - 1];
        //console.log(JSON.stringify(configFs.roleReaction.splice(args[1]-1,1)));
        const filter = m => m.author.id = message.author.id;
        const role = message.guild.roles.cache.find(r => r.id === rr.roleId);
        message.channel.send(`Êtes vous sur de vouloir suprimer ce reaction-role ? (répondez \`oui\` ou \`non\` en moins de 30s)\n${role.name} -> emoji : ${rr.emojiName} message : https://discord.com/channels/${rr.path}`).then(() => {
            message.channel.awaitMessages(filter, { max: 1, time: 30000, errors: ['time'] })
                .then(collected => {
                    if (collected.first().content === "oui") {
                        configFs.roleReaction.splice(args[1] - 1, 1);
                        //return console.log(JSON.stringify(data));
                        fs.writeFile("./plugins/roleReaction/config.json", JSON.stringify(configFs), async (err) => {
                            if (err) {
                                console.log(err);
                                message.channel.send(`❌ Je n'ai pas réussi a ajouter le role-réaction.`);
                            }
                            else {
                                message.channel.send(`Ce reaction-role a été suprimé`);
                            }
                        });

                    } else {
                        message.channel.send('❌ Commande annulé');
                    }
                })
                .catch(collected => {
                    message.channel.send('❌ Commande annulé');
                });
        });
    },
    modify = (client, message, args) => {
        return message.reply(" commande indisponible pour le moment...");
        if (args.length === 4) return message.reply(' syntaxte incorecte : `role-reaction modify <role-reaction index> [-e <new emoji>] [-p <new discord link>] [-r <new role-id>]`');

    }
];

module.exports.help = {
    name: "role-reaction",
    tag: "base",
    desc: "Send a message with the bot",
    aliases: ["reaction-role", "r-r", "r-rc", "rc-r"],
    subcommands: ["list", "add", "remove", "modify"],
    usage: "<list|add|remove|modify>"
}