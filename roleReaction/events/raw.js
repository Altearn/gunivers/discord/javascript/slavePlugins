const fs = require("fs");

module.exports = async (client, event) => {
    if (event.t !== "MESSAGE_REACTION_ADD" && event.t !== "MESSAGE_REACTION_REMOVE") return;
    const config = JSON.parse(fs.readFileSync('./plugins/roleReaction/config.json'));
    const array = config.roleReaction;
    const guild = client.guilds.cache.get(event.d.guild_id);
    //console.log(array);
    const member = guild.members.fetch(event.d.user_id).then(m => {
        array.forEach(async roleReaction => {
            const path = roleReaction.path.split("/");
            const messageId = path[path.length - 1]
            if (event.d.message_id === messageId && event.d.emoji.name === roleReaction.emojiName) {
                try {
                    if (event.t == "MESSAGE_REACTION_ADD") await m.roles.add(roleReaction.roleId, "role-réaction");
                    if (event.t == "MESSAGE_REACTION_REMOVE") await m.roles.remove(roleReaction.roleId, "role-réaction");
                } catch {
                    console.log("Je n'ai pas réussi a mettre le role");
                }
            }
        });
    }).catch(err => {
        console.log(err);
    });
}