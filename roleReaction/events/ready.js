const config = require("../config.json");
module.exports = async (client) => {
    const array = config.roleReaction;
    array.forEach(async roleReaction => {
        const path = roleReaction.path.split("/");
        const guildId = path[0]
        const channelId = path[1];
        const messageId = path[path.length - 1]
        let message = await client.guilds.cache.get(guildId).channels.cache.get(channelId).messages.fetch(messageId);//.react(roleReaction.emojiName);
        //return console.log(message);
        message.react(roleReaction.emojiName);
        //client.guilds.fetch(guildId).then(guild => guild.channels.fetch(channelId).then(channel => channel.send("test")));

    });
}
